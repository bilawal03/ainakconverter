var express = require('express');
var router = express.Router();
const fs = require('fs');
const os = require('os');
const cmd = require('node-cmd');
const rimraf = require('rimraf');
const Promise = require('bluebird');
const path = require('path');

// to upload file
var multer  = require('multer');
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'public/images')
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname)
  }
})
var upload = multer({ storage: storage })


router.get('/', function(req, res, next) {
  res.render('index', { title: 'Ainak' });
});

router.post('/upload',upload.single('upfile'),function(req,res) {
  console.log(req.file);
  // res.send("SUP")
  //
  var file = req.file;

  let srcFile = file.path;
  res.download(path.join(__dirname, '../public/images', file.fileName));
  // let destFile = file.path + '-ainak.glb';
  //
  // let binExt = os.type() === 'Windows_NT' ? '.exe' : '';
  // let tool = path.join(__dirname, '../exes', os.type(), 'FBX2glTF' + binExt);
  //
  // console.log(srcFile, destFile, tool);
  // if (!fs.existsSync(tool)) {
  //   throw new Error(`Unsupported OS: ${os.type()}`);
  // }
  // let destExt;
  // if (destFile.endsWith('.glb')) {
  //   destExt = '.glb';
  // } else if (destFile.endsWith('.gltf')) {
  //   destExt = '.gltf';
  // } else {
  //   throw new Error(`Unsupported file extension: ${destFile}`);
  // }
  //
  // let srcPath = fs.realpathSync(srcFile);
  // let destDir = fs.realpathSync(path.dirname(destFile));
  // let destPath = path.join(destDir, path.basename(destFile, destExt));
  //
  // let args = ' -b [--khr-materials-common] --input ' + srcPath + ' --output ' + destPath;
  //
  // const getAsync = Promise.promisify(cmd.get, {
  //   multiArgs: true,
  //   context: cmd
  // });
  //
  // try {
  //   getAsync(tool + args).then(data => {
  //     console.log("done!");
  //     // the FBX SDK may create an .fbm dir during conversion; delete!
  //     let fbmCruft = srcPath.replace(/.fbx$/i, '.fbm');
  //     let fbmCruft_orig = srcPath.replace(/.fbx$/i, '.fbx');
  //     // don't stick a fork in things if this fails, just log a warning
  //     const onError = error =>
  //         console.warn(`Failed to delete ${fbmCruft}: ${error}`);
  //     try {
  //       fs.existsSync(fbmCruft) && rimraf(fbmCruft, {}, onError);
  //       fs.existsSync(fbmCruft_orig) && rimraf(fbmCruft_orig, {}, onError);
  //     } catch (error) {
  //       console.warn(`Failed to delete ${fbmCruft}: ${error}`);
  //     }
  //
  //     res.download(destPath + destExt)
  //   }).catch(err => {
  //     res.send(err);
  //   })
  // } catch (error) {
  //   res.send("Main Error")
  // }




  // res.send('Done! Uploading files')
});

module.exports = router;
